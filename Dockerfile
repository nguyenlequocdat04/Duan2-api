FROM tensorflow/tensorflow:1.13.0rc0-py3

COPY requirements.txt /
RUN pip install --upgrade pip \
    && pip install -r requirements.txt \
    && apt-get update -y \
    && apt-get install libgtk2.0-dev -y
COPY . /mnt/api
WORKDIR /mnt/api
