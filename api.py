from app import app
from app.detector import DetectImgV2 #load model when app starts

if __name__ == '__main__':
    DetectImgV2(None) #load model
    app.run(host = '0.0.0.0',port=5000, debug=True)