import os
from imageai.Detection.Custom import CustomObjectDetection

path = 'images/test/img'
rs = 'images/test/result'
test_imgs = os.listdir(path)

detector = CustomObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath("{}".format('/mnt/api/app/detect-model/detection_model-ex-006--loss-0003.670.h5'))
detector.setJsonPath("{}".format('/mnt/api/app/detect-model/detection_model-ex-006--loss-0003.670.json'))
detector.loadModel()

for img in test_imgs:
    print(img)
    detections = detector.detectObjectsFromImage(input_image="{img_path}/{img_name}".format(img_path=path, img_name=img), output_image_path="{result_dir}/{img_name}".format(result_dir=rs, img_name=img))
    for detection in detections:
            print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
