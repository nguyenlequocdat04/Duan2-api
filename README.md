# Duan2-api

**Change path in app/config.py**
* Tải model .h5 và json file đặt trong detect-model
## run by docker
* `docker-compose up -d`
* `docker exec -it Duan2-api bash`
    - API: 
        - Open: http://localhost/5000 to see docs
    - For test: 
        - **Change path in file**
        - `python scripts/test_model.py`

## run by python
* `python -m venv venv`
* `venv\Scripts\activate`
* `pip install tensorflow==1.13.1`
* `pip install -r requirements.txt`
- **Chỉnh path trong file config `MODEL_PATH`, `MODEL_JSON`, `UPLOAD_FOLDER`, `RESULT_FOLDER`**
    - API: 
        - `python api.py`
        - Open: http://localhost/5000 to see docs
    - For test: 
        - **Change path in file**
        - `python scripts/test_model.py`