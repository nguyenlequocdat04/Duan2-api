import os
from flask import Flask, request, redirect, url_for, send_from_directory, render_template, flash
from flask_restplus import Resource, Api, Namespace
from werkzeug.utils import secure_filename
from app import app
from app.detector import DetectImg, DetectImgV2
api = Api(app)

@api.route('/hello')
class HelloWorld(Resource):
    def get(self):
        # return {'hello': 'world'}
        return [
                    {
                        "score": 99.80818629264832,
                        "result": "banh mi",
                        "box": [
                            258,
                            11,
                            359,
                            71
                        ]
                    },
                    {
                        "score": 96.09472155570984,
                        "result": "com tam",
                        "box": [
                            165,
                            113,
                            253,
                            181
                        ]
                    }
                ]


def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1].lower(
            ) in app.config['ALLOWED_EXTENSIONS']


@api.route('/upload_img')
class UploadImage(Resource):
    def post(self):
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return {'error':'can not get file'}
        file = request.files['file']
        if not file:
            return {'error':'can not get file'}
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            result_arr = DetectImg(filename)
            return result_arr
        return {'error':'some error'}

@app.route('/result/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['RESULT_FOLDER'],
                                filename)


@app.route('/detect_image/<filename>')
def detect_image(filename):
    print('file uploaded: {filename}'.format(filename = filename))
    result_arr = DetectImg(filename)
    # return send_from_directory(app.config['RESULT_FOLDER'], filename)
    # src_image = "http://localhost:5000/result/{filename}".format(filename=filename) #need to change
    # return render_template('upload.html', src_image=src_image)
    return result_arr

@api.route('/upload_img_v2')
class UploadImageV2(Resource):
    def post(self):
        """
        Return result and base64 image
        """
        # Return:
        # {
        #     "object": [
        #         {
        #             "result": "com tam",
        #             "score": 99.93032217025757
        #         }
        #     ],
        #     "image":"/9jBRIhMUEGE1FhQ0RFRkdISUpTVFVWV1hZWmNk......"
        # }
        
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return {'error':'can not get file'}
        file = request.files['file']
        if not file:
            return {'error':'can not get file'}
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            result_arr, base64_str_image = DetectImgV2(filename)
            # return [result_arr, base64_str_image]
            return {
                "object": result_arr,
                "image": base64_str_image
            }
        return {'error':'some error'}