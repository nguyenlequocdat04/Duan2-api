class DefaultConfig():
    MODEL_PATH = '/mnt/api/app/detect-model/detection_model-ex-006--loss-0003.670.h5'
    MODEL_JSON = '/mnt/api/app/detect-model/detection_model-ex-006--loss-0003.670.json'
    UPLOAD_FOLDER = '/mnt/api/images/upload'
    RESULT_FOLDER = '/mnt/api/images/result'
    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg'}
    BASE_WIDTH = 600