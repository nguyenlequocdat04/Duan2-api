from imageai.Detection.Custom import CustomObjectDetection
from app.config import DefaultConfig
import os
from PIL import Image
from io import BytesIO
import base64
import numpy as np
import cv2
import tensorflow as tf

img_path = DefaultConfig.UPLOAD_FOLDER
result_dir = DefaultConfig.RESULT_FOLDER
basewidth = DefaultConfig.BASE_WIDTH
model_isloaded = False # not loaded
detector = None
graph = None
# ==========
def load_model():
    global detector, model_isloaded
    global graph
    if not model_isloaded:
        detector = CustomObjectDetection()
        detector.setModelTypeAsYOLOv3()
        detector.setModelPath("{}".format(DefaultConfig.MODEL_PATH))
        detector.setJsonPath("{}".format(DefaultConfig.MODEL_JSON))
        detector.loadModel()
        graph = tf.get_default_graph()
        model_isloaded = True
        print("Loaded model!!")
    return detector
# ===========
# def DetectImg(img_name):
#     global detector
#     if(img_name is not None):
#         if not model_isloaded:
#             load_model()

#         detections = detector.detectObjectsFromImage(input_image="{img_path}/{img_name}".format(img_path=img_path, img_name=img_name), output_image_path="{result_dir}/{img_name}".format(result_dir=result_dir, img_name=img_name))
        
#         for detection in detections:
#             print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
#     else:
#         if(not model_isloaded):
#             LoadModel() 
#         return None 

def DetectImg(img_name):
    global detector
    result_arr = []
    if(img_name is not None):
        if not model_isloaded:
            load_model()

        detections = detector.detectObjectsFromImage(input_image="{img_path}/{img_name}".format(img_path=img_path, img_name=img_name), output_image_path="{result_dir}/{img_name}".format(result_dir=result_dir, img_name=img_name))
        
        for detection in detections:
            detect_result = {
                "result": detection["name"],
                "score": detection["percentage_probability"],
                "box": detection["box_points"],
            }
            print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
            result_arr.append(detect_result)
        return result_arr
    else:
        if(not model_isloaded):
            LoadModel()
        return None

def DetectImgV2(img_name):
    global detector
    result_arr = []
    img_name = "{img_path}/{img_name}".format(img_path=img_path, img_name=img_name)
    if img_name and resize_img(img_name):
        if not model_isloaded:
            load_model()

        # detected_image_array, detections = detector.detectObjectsFromImage(output_type="array", input_image="{img_path}/{img_name}".format(img_path=img_path, img_name=img_name))
        with graph.as_default():
            detected_image_array, detections = detector.detectObjectsFromImage(output_type="array", input_image=img_name, minimum_percentage_probability=70)
        for detection in detections:
            detect_result = {
                "result": detection["name"],
                "score": detection["percentage_probability"],
                # "box": detection["box_points"],
            }
            print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
            result_arr.append(detect_result)
        detected_image_array = cv2.cvtColor(detected_image_array, cv2.COLOR_BGR2RGB)   
        # detected_image_array = np.asarray(detected_image_array,dtype=np.uint8)
        pil_img = Image.fromarray(detected_image_array)
        # pil_img.show()
        buff = BytesIO()
        pil_img.save(buff, format="JPEG")
        base64_str_image = base64.b64encode(buff.getvalue()).decode('utf-8')
        return result_arr, base64_str_image
    else:
        if(not model_isloaded):
            load_model()
        return None

def resize_img(img_name):
    try:
        img = Image.open(img_name)
        wpercent = (basewidth/float(img.size[0]))
        hsize = int((float(img.size[1])*float(wpercent)))
        img = img.resize((basewidth,hsize), Image.ANTIALIAS)
        img.save(img_name)
        return True
    except:
        return False